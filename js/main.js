var app = (function () {

    var _init = function () {
        _initMainSlider();
        _initTrendingItemsSlider();
        _initBlogSlider();
        _initOnScrollEvents();
    };

    var _initOnScrollEvents = function () {
        window.onscroll = () => setStickyHeader();
    };

    var _initMainSlider = function () {
        const swiper = new Swiper('.swiper-container', {
            // Optional parameters
            direction: 'horizontal',
            loop: false,
            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
                renderBullet: function (index, className) {
                    return '<span class="slider__bullet ' + className + '"></span>';
                },
            }

        });
    };

    var _initTrendingItemsSlider = function () {
        const swiper = new Swiper('.trending-items__swiper-container', {
            // Optional parameters
            direction: 'horizontal',
            loop: false,
            // If we need pagination
            pagination: {
                el: '.trending-items__pagination',
                clickable: true,
                renderBullet: function (index, className) {
                    return '<span class="trending-items__bullet ' + className + '"></span>';
                },
            }

        });
    };

    var setStickyHeader = function () {
        const navbar = document.getElementById("header");
        const sticky = navbar.offsetTop;

        if (window.pageYOffset > sticky) {
            navbar.classList.add("header--sticky")
        } else {
            navbar.classList.remove("header--sticky");
        }
    };

    var _initBlogSlider = function () {
        const swiper = new Swiper('.blog__swiper-container', {
            // Optional parameters
            direction: 'horizontal',
            slidesPerView: 1,
            spaceBetween: 30,
            slidesPerGroup: 1,
            loop: true,
            // If we need pagination
            pagination: {
                el: '.blog__pagination',
                clickable: true,
                renderBullet: function (index, className) {
                    return '<span class="blog__bullet ' + className + '"></span>';
                },
            },
            navigation: {
                nextEl: '.blog__button-next',
                prevEl: '.blog__button-prev',
            },
            breakpoints: {
                600: {
                    slidesPerView: 2,
                    spaceBetweenSlides: 30
                }
            }

        });
    };

    return {
        init: _init,
    }
})();

document.addEventListener('DOMContentLoaded', function() {
    app.init();
});
